﻿using UnityEngine;

class SlotLivro1: MonoBehaviour
{
    public static bool isFitted = false;
    public bool hasBook = false;
    public GameObject requiredBook;
    public Transform slotPos;

    public void Update()
    {
        //Verificação constante pelo livro
        for (int i = 0; i < Inventory.aItens.Count; i++)
        {
            if (Inventory.aItens[i].name == requiredBook.name) { hasBook = true; }
        }

        if (hasBook)
        {

            if (!isFitted)
            {
                if (ActionContext.conActive && DistanceTrigger.triggeredObj.name == "Slot Livro 1")
                {
                    isFitted = true;
                    ActionContext.conActive = false;

                    GameObject.Find("Locked Door Book").GetComponent<LockedDoor>().hasKey = true;
                    GameObject.Find("Locked Door Book").GetComponent<LockedDoor>().isUnlocked = true;
                    
                    //Posicionar Livro no Slot
                    requiredBook.transform.position = slotPos.transform.position;
                    requiredBook.transform.rotation = slotPos.transform.rotation;
                    requiredBook.SetActiveRecursively(true);
                    requiredBook.name = requiredBook.name + " STATIC";

                    //Tirar da Array do Inventário
                    for (int i = 0; i < Inventory.aItens.Count; i++)
                    {
                        if (Inventory.aItens[i].name == requiredBook.name) { Inventory.aItens.RemoveAt(i); Inventory.aItens.TrimExcess(); }
                    }

                }
            }
        }
    }
}