﻿using UnityEngine;

class SlotLivro2 : MonoBehaviour
{
    public static bool isFitted = false;
    public bool hasBook = false;
    public GameObject requiredBook;
    public Transform slotPos;
    public GameObject Papel3;

    public void Start()
    {
        Papel3.SetActiveRecursively(false);
    }

    public void Update()
    {
        //Verificação constante pelo livro
        for (int i = 0; i < Inventory.aItens.Count; i++)
        {
            if (Inventory.aItens[i].name == requiredBook.name) { hasBook = true; }
        }

        if (hasBook)
        {

            if (!isFitted)
            {
                if (ActionContext.conActive && DistanceTrigger.triggeredObj.name == "Slot Livro 2")
                {
                    isFitted = true;
                    ActionContext.conActive = false;

                    Papel3.SetActiveRecursively(true);

                    //Posicionar Livro no Slot
                    requiredBook.transform.position = slotPos.transform.position;
                    requiredBook.transform.rotation = slotPos.transform.rotation;
                    requiredBook.SetActiveRecursively(true);
                    requiredBook.name = requiredBook.name + " STATIC";

                    //Tirar da Array do Inventário
                    for (int i = 0; i < Inventory.aItens.Count; i++)
                    {
                        if (Inventory.aItens[i].name == requiredBook.name) { Inventory.aItens.RemoveAt(i); Inventory.aItens.TrimExcess(); }
                    }

                }
            }
        }
    }
}