﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
class ActionContext : MonoBehaviour
{
    public static bool conActive;

    public void Update()
    {

        if (!Inventory.isOpened && !EscapeMenu.isOpened)
        {
            if (Input.GetMouseButtonDown(0) && DistanceTrigger.colliding && !conActive)
            {
                if (DistanceTrigger.inspectable)
                {
                    conActive = true;
                }
                else
                {
                    Inventory.aItens.Add(DistanceTrigger.triggeredObj);
                    if (DistanceTrigger.triggeredObj.name == "Livro 2")
                    {
                        Inventory.aItens.Add(GameObject.Find("Chave do Livro 2"));
                    }
                    DistanceTrigger.triggeredObj.SetActiveRecursively(false);
                    DistanceTrigger.colliding = false;
                    GameObject.Find("Distance Trigger").GetComponent<DistanceTrigger>().conText.text = "";
                }
            }
            else if (Input.GetKeyDown(KeyCode.Escape) && conActive)
            {
                conActive = false;
            }

            CharacterMotor.canControl = !conActive;
            MouseLook.canUpdate = !conActive;
            Screen.showCursor = conActive;
        }
    }
}