﻿using UnityEngine;

class LockedDoor : MonoBehaviour
{
    public bool isUnlocked = false;
    public bool hasKey = false;
    public GameObject keyObj;
    public bool isOpened = false;
    public float closingDist = 5;
    public Transform playerPos;
    public GameObject doorTriggered;

    public AudioClip Abrir;
    public AudioClip Fechar;
    public AudioClip Destrancar;

    public void Update()
    {
        //Novo vetor de posição relativa para verificar a distância da porta aberta (gameObject) com o jogador:
        Vector3 dist = playerPos.position - gameObject.transform.position;

        //Verificação constante pela chave
        for (int i = 0; i < Inventory.aItens.Count; i++)
        {
            if (Inventory.aItens[i].name == keyObj.name){hasKey = true;}
        }

        if (hasKey)
        {

            if (!isOpened)
            {
                if (ActionContext.conActive && (DistanceTrigger.triggeredObj.name == "Locked Door" || DistanceTrigger.triggeredObj.name == "Locked Door Book"))
                {
                    doorTriggered = DistanceTrigger.triggeredObj;
                    doorTriggered.animation.Play("door_open");
                    doorTriggered.GetComponent<AudioSource>().PlayOneShot(Abrir);
                    ActionContext.conActive = false;
                    doorTriggered.GetComponent<LockedDoor>().isOpened = true;
                    if (!isUnlocked) { isUnlocked = true; doorTriggered.GetComponent<AudioSource>().PlayOneShot(Destrancar);}
                }
            }

            if (isOpened)
            {
                if (dist.magnitude > closingDist)
                {
                    gameObject.animation.Play("door_close");
                    gameObject.GetComponent<AudioSource>().PlayOneShot(Fechar);
                    gameObject.GetComponent<LockedDoor>().isOpened = false;
                    doorTriggered = null;
                }
            }

        }
    }
}