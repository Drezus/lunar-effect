﻿using UnityEngine;

class Switch1 : MonoBehaviour
{
    public bool isOn = false;
    public AudioClip Som;

    public void Update()
    {
        if (SlotLivro1.isFitted && !LightSolution.isCompleted)
        {

            //Ligar-desligar luzes
            if (ActionContext.conActive && DistanceTrigger.triggeredObj.name == "Switch 1")
            {
                ActionContext.conActive = false;
                DistanceTrigger.triggeredObj.GetComponent<AudioSource>().PlayOneShot(Som);

                if (isOn)
                {
                    isOn = false;
                }
                else
                {
                    isOn = true;
                }

                //Ligar/desligar as luzes especificamente afetadas por Switch 1
                GameObject.Find("Luz 1").GetComponent<Light>().enabled = !GameObject.Find("Luz 1").GetComponent<Light>().enabled;
                GameObject.Find("Luz 2").GetComponent<Light>().enabled = !GameObject.Find("Luz 2").GetComponent<Light>().enabled;
                GameObject.Find("Luz 3").GetComponent<Light>().enabled = !GameObject.Find("Luz 3").GetComponent<Light>().enabled;
                GameObject.Find("Luz 4").GetComponent<Light>().enabled = !GameObject.Find("Luz 4").GetComponent<Light>().enabled;
                GameObject.Find("Luz 5").GetComponent<Light>().enabled = !GameObject.Find("Luz 5").GetComponent<Light>().enabled;
                GameObject.Find("Luz 6").GetComponent<Light>().enabled = !GameObject.Find("Luz 6").GetComponent<Light>().enabled;
                GameObject.Find("Luz 7").GetComponent<Light>().enabled = !GameObject.Find("Luz 7").GetComponent<Light>().enabled;
                GameObject.Find("Luz 8").GetComponent<Light>().enabled = !GameObject.Find("Luz 8").GetComponent<Light>().enabled;
                GameObject.Find("Luz 10").GetComponent<Light>().enabled = !GameObject.Find("Luz 10").GetComponent<Light>().enabled;
                GameObject.Find("Luz 13").GetComponent<Light>().enabled = !GameObject.Find("Luz 13").GetComponent<Light>().enabled;
                GameObject.Find("Luz 14").GetComponent<Light>().enabled = !GameObject.Find("Luz 14").GetComponent<Light>().enabled;
                GameObject.Find("Luz 15").GetComponent<Light>().enabled = !GameObject.Find("Luz 15").GetComponent<Light>().enabled;                
            }
        }

    }
}


//GameObject.Find("Luz 1").GetComponent<Light>().enabled = !GameObject.Find("Luz 1").GetComponent<Light>().enabled;
//GameObject.Find("Luz 2").GetComponent<Light>().enabled = !GameObject.Find("Luz 2").GetComponent<Light>().enabled;
//GameObject.Find("Luz 3").GetComponent<Light>().enabled = !GameObject.Find("Luz 3").GetComponent<Light>().enabled;
//GameObject.Find("Luz 4").GetComponent<Light>().enabled = !GameObject.Find("Luz 4").GetComponent<Light>().enabled;
//GameObject.Find("Luz 5").GetComponent<Light>().enabled = !GameObject.Find("Luz 5").GetComponent<Light>().enabled;
//GameObject.Find("Luz 6").GetComponent<Light>().enabled = !GameObject.Find("Luz 6").GetComponent<Light>().enabled;
//GameObject.Find("Luz 7").GetComponent<Light>().enabled = !GameObject.Find("Luz 7").GetComponent<Light>().enabled;
//GameObject.Find("Luz 8").GetComponent<Light>().enabled = !GameObject.Find("Luz 8").GetComponent<Light>().enabled;
//GameObject.Find("Luz 9").GetComponent<Light>().enabled = !GameObject.Find("Luz 9").GetComponent<Light>().enabled;
//GameObject.Find("Luz 10").GetComponent<Light>().enabled = !GameObject.Find("Luz 10").GetComponent<Light>().enabled;
//GameObject.Find("Luz 11").GetComponent<Light>().enabled = !GameObject.Find("Luz 11").GetComponent<Light>().enabled;
//GameObject.Find("Luz 12").GetComponent<Light>().enabled = !GameObject.Find("Luz 12").GetComponent<Light>().enabled;
//GameObject.Find("Luz 13").GetComponent<Light>().enabled = !GameObject.Find("Luz 13").GetComponent<Light>().enabled;
//GameObject.Find("Luz 14").GetComponent<Light>().enabled = !GameObject.Find("Luz 14").GetComponent<Light>().enabled;
//GameObject.Find("Luz 15").GetComponent<Light>().enabled = !GameObject.Find("Luz 15").GetComponent<Light>().enabled;
//GameObject.Find("Luz 16").GetComponent<Light>().enabled = !GameObject.Find("Luz 16").GetComponent<Light>().enabled;
