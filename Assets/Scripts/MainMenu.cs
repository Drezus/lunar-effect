﻿using UnityEngine;

class MainMenu : MonoBehaviour
{
    public GUISkin mainMenuSkin;
    public Camera mainMenuCamera;
    public Camera fpsCamera;
    public static bool isOpened = true;
    public GUITexture logo;
    public GUITexture credits;
    public bool isCreditsOn = false;

    public void Update()
    {        
        //Desligar todo o movimento e ligar o mouse
        CharacterMotor.canControl = false;
        MouseLook.canUpdate = false;
        Screen.showCursor = true;    
        mainMenuCamera.enabled = true;
        fpsCamera.enabled =  false;
        logo.enabled = true;

        if (!isCreditsOn)
        {
            logo.enabled = true;
            credits.enabled = false;
        }
        else
        {
            logo.enabled = false;
            credits.enabled = true;
        }
    }

    //Menu Principal
    public void OnGUI()
    {
        if (isOpened)
        {
            GUI.skin = mainMenuSkin;

            //Botões
            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2, 300, 100), "Iniciar"))
            {
                isOpened = false;

                CharacterMotor.canControl = true;
                MouseLook.canUpdate = true;
                Screen.showCursor = false;

                mainMenuCamera.enabled = false;
                fpsCamera.enabled = true;
                logo.enabled = false;

                gameObject.SetActiveRecursively(false);
            }

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 1.5f, 300, 100), "Créditos"))
            {
                //Mostrar Créditos
                isCreditsOn = !isCreditsOn;
            }

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 1.2f, 300, 100), "Sair"))
            {
                Application.Quit();
            }          

        }

    }
}