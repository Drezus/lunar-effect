﻿using UnityEngine;

class MusicManager : MonoBehaviour
{
    public bool isPlaying;

    public void Update()
    {
        if (!MainMenu.isOpened && !isPlaying)
        {
            audio.Play();
            isPlaying = true;
        }
    }
}