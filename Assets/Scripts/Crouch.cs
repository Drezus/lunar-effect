﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
class Crouch : MonoBehaviour
{
    public Transform MainPos;
    public Transform CrouchPos;

    public void Update()
    {
        if (!ActionContext.conActive && !Inventory.isOpened && !EscapeMenu.isOpened)
        {
            if (Input.GetKey(KeyCode.LeftControl))
            { 
                GameObject.Find("Main Camera").transform.position = CrouchPos.transform.position;
            }
            else
            {
                GameObject.Find("Main Camera").transform.position = MainPos.transform.position;
            }
        }
    }


}