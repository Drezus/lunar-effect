﻿using UnityEngine;

class DistanceTrigger : MonoBehaviour
{
    public GUIText conText;
    public static bool colliding = false;
    public static bool inspectable = false;
    public static GameObject triggeredObj = null;

    public void Start()
    {
        conText.text = "";
    }

    public void OnTriggerStay(Collider other)
    {
        if (!ActionContext.conActive && !Inventory.isOpened && !EscapeMenu.isOpened)
        {
            //Compartilhar o objeto sendo tocado static-amente para outros scripts que precisarem
            triggeredObj = other.gameObject;


            //Qual o objeto? - Identificar que objeto é e imprimir a ação correspondente.
            //Ele é interágivel ou coletável? - Identificar o tipo de ação que será tomada ao pressionar o mouse em ActionContext.
            if (other.gameObject.name == "Livro 1")
            {
                conText.text = "Coletar Livro";
                colliding = true;
                inspectable = false;
            }
            else if (other.gameObject.name == "Livro 2")
            {
                conText.text = "Coletar Livro";
                colliding = true;
                inspectable = false;
            }
            else if (other.gameObject.name == "Livro 3")
            {
                conText.text = "Coletar Livro";
                colliding = true;
                inspectable = false;
            }
            else if (other.gameObject.name == "Pedaço de Papel 1")
            {
                conText.text = "Coletar";
                colliding = true;
                inspectable = false;
            }
            else if (other.gameObject.name == "Pedaço de Papel 2")
            {
                conText.text = "Coletar";
                colliding = true;
                inspectable = false;
            }
            else if (other.gameObject.name == "Pedaço de Papel 3")
            {
                conText.text = "Coletar";
                colliding = true;
                inspectable = false;
            }
            else if (other.gameObject.name == "Chave Mestra")
            {
                conText.text = "Coletar Chave";
                colliding = true;
                inspectable = false;
            }



            else if (other.gameObject.name == "Door" && triggeredObj.GetComponent<OpenDoor>().isOpened == false)
            {
                conText.text = "Abrir Porta";
                colliding = true;
                inspectable = true;
            }



            else if ((other.gameObject.name == "Locked Door" || other.gameObject.name == "Locked Door Book") && triggeredObj.GetComponent<LockedDoor>().isOpened == false)
            {
                if (triggeredObj.GetComponent<LockedDoor>().hasKey == false && triggeredObj.GetComponent<LockedDoor>().isUnlocked == false)
                {
                    conText.text = "*TRANCADA*";
                    colliding = false;
                    inspectable = false;
                }
                else if (triggeredObj.GetComponent<LockedDoor>().hasKey == true && triggeredObj.GetComponent<LockedDoor>().isUnlocked == false)
                {
                    conText.text = "Destrancar Porta";
                    colliding = true;
                    inspectable = true;
                }
                else if (triggeredObj.GetComponent<LockedDoor>().hasKey == true && triggeredObj.GetComponent<LockedDoor>().isUnlocked == true)
                {
                    conText.text = "Abrir Porta";
                    colliding = true;
                    inspectable = true;
                }
                
            }


            else if (other.gameObject.name == "Locked Drawer" && triggeredObj.GetComponent<LockedDrawer>().isOpened == false)
            {
                if (triggeredObj.GetComponent<LockedDrawer>().hasKey == false && triggeredObj.GetComponent<LockedDrawer>().isUnlocked == false)
                {
                    conText.text = "*TRANCADA*";
                    colliding = false;
                    inspectable = false;
                }
                else if (triggeredObj.GetComponent<LockedDrawer>().hasKey == true && triggeredObj.GetComponent<LockedDrawer>().isUnlocked == false)
                {
                    conText.text = "Destrancar Gaveta";
                    colliding = true;
                    inspectable = true;
                }
                else if (triggeredObj.GetComponent<LockedDrawer>().hasKey == true && triggeredObj.GetComponent<LockedDrawer>().isUnlocked == true)
                {
                    conText.text = "Abrir Gaveta";
                    colliding = true;
                    inspectable = true;
                }
            }



            else if (other.gameObject.name == "Slot Livro 1" && SlotLivro1.isFitted == false)
            {
                if (triggeredObj.GetComponent<SlotLivro1>().hasBook == false && SlotLivro1.isFitted == false)
                {
                    conText.text = "";
                    colliding = false;
                    inspectable = false;
                }
                else if (triggeredObj.GetComponent<SlotLivro1>().hasBook == true && SlotLivro1.isFitted == false)
                {
                    conText.text = "Encaixar Livro 1";
                    colliding = true;
                    inspectable = true;
                }
                else if (triggeredObj.GetComponent<SlotLivro1>().hasBook == true && SlotLivro1.isFitted == true)
                {
                    conText.text = "";
                    colliding = true;
                    inspectable = true;
                }
            }

            else if (other.gameObject.name == "Slot Livro 2" && SlotLivro2.isFitted == false)
            {
                if (triggeredObj.GetComponent<SlotLivro2>().hasBook == false && SlotLivro2.isFitted == false)
                {
                    conText.text = "";
                    colliding = false;
                    inspectable = false;
                }
                else if (triggeredObj.GetComponent<SlotLivro2>().hasBook == true && SlotLivro2.isFitted == false)
                {
                    conText.text = "Encaixar Livro 2";
                    colliding = true;
                    inspectable = true;
                }
                else if (triggeredObj.GetComponent<SlotLivro2>().hasBook == true && SlotLivro2.isFitted == true)
                {
                    conText.text = "";
                    colliding = true;
                    inspectable = true;
                }
            }

            else if (other.gameObject.name == "Slot Livro 3" && SlotLivro3.isFitted == false)
            {
                if (triggeredObj.GetComponent<SlotLivro3>().hasBook == false && SlotLivro3.isFitted == false)
                {
                    conText.text = "";
                    colliding = false;
                    inspectable = false;
                }
                else if (triggeredObj.GetComponent<SlotLivro3>().hasBook == true && SlotLivro3.isFitted == false)
                {
                    conText.text = "Encaixar Livro 3";
                    colliding = true;
                    inspectable = true;
                }
                else if (triggeredObj.GetComponent<SlotLivro3>().hasBook == true && SlotLivro3.isFitted == true)
                {
                    conText.text = "";
                    colliding = true;
                    inspectable = true;
                }
            }


            else if (other.gameObject.name == "Switch 1" && SlotLivro1.isFitted && !LightSolution.isCompleted)
            {
                conText.text = "Pressionar Interruptor";
                colliding = true;
                inspectable = true;
            }
            else if (other.gameObject.name == "Switch 2" && SlotLivro2.isFitted && !LightSolution.isCompleted)
            {
                conText.text = "Pressionar Interruptor";
                colliding = true;
                inspectable = true;
            }
            else if (other.gameObject.name == "Switch 3" && SlotLivro3.isFitted && !LightSolution.isCompleted)
            {
                conText.text = "Pressionar Interruptor";
                colliding = true;
                inspectable = true;
            }
            else if (other.gameObject.name == "Switch 4" && SlotLivro3.isFitted && !LightSolution.isCompleted)
            {
                conText.text = "Pressionar Interruptor";
                colliding = true;
                inspectable = true;
            }


            else
            {
                conText.text = "";
                colliding = false;
                inspectable = false;
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        triggeredObj = null;
        conText.text = "";
        colliding = false;
        inspectable = false;
        ActionContext.conActive = false;
    }
}



//***COISAS VELHAS***
//if (other.gameObject.name == "Box" && !BoxPuzzle.completed)
//{
//    conText.text = "Inspect Box";
//    colliding = true;
//    inspectable = true;
//}
//else if (other.gameObject.name == "Door Key")
//{
//    conText.text = "Collect Key";
//    colliding = true;
//    inspectable = false;
//}