﻿using UnityEngine;
using System.Collections.Generic;

class Inventory : MonoBehaviour
{
    public static bool isOpened = false;
    public static List<GameObject> aItens;
    public int maxItens = 12;
    public string tooltip;
    public GUISkin titanicSkin;
    public int timer = 0;
    public static bool countDialogue;
    public GUIText MadeleineDialogue;

    //Descrição dos Itens
    private string Livro1Tip = "Contém uma página marcada, com os seguintes dizeres: “A Lua guia o caminho da fertilidade, a força feminina vem dela, após quatro dúzias de luas novas, sangue fresco de um touro deve ser derramado. Os olhos dela não se abrirão sem o sangue derramado, e se os olhos dela não se abrirem elas não terão mais poder.”. A exótica capa dura é adornada pelo número 1.";
    private string Livro2Tip = "Uma seção do livro marca o seguinte trecho: “O prata quando acende o inverso ocorrera. Se Ela não for satisfeita nunca mais serão fortes. A mulher das mulheres. A Deusa da vida. A Losna.”. A capa indica, claramente, o número 2.";
    private string Livro3Tip = "Uma das páginas sobresaltava devido a uma chave no meio do livro. Essa página descrevia: “Apenas do leste ao oeste a luz pode ser feita. De seus raios pratas o caminho verá e de seu feitio o caminho mostrará. A chave: com ela, todos os caminhos se abrirão.”. A capa tem, em detalhes, o número 3 impresso.";

    private string Papel1Tip = "Claramente, um pedaço de papel ensanguentado...";
    private string Papel2Tip = "Mais um pedaço de papel ensanguentado. Começa a parecer um lugar familiar...";
    private string Papel3Tip = "Parece ser o pedaço restante de uma planta baixa de uma das salas do navio.";
    
    private string ChaveLivro2Tip = "Uma pequena chave encontrada dentro do segundo livro. Ainda não se sabe a sua utilidade, mas não parece ser grande o suficiente para uma fechadura de uma porta comum.";
    private string MasterChaveTip = "Uma grande e pesada chave que parece ter sido usada muitas vezes. É possível ver um encaixe na parte de trás.";


    public void Start()
    {
        isOpened = false;
        MadeleineDialogue.text = "";
        countDialogue = true;

        //Inicialização Array
        aItens = new List<GameObject>();

    }

    public void Update()
    {
        //Texto Inicial
        if (!MainMenu.isOpened && countDialogue)
        {
            timer += 1;

            if (timer >= 0 && timer < 120)
            {
                MadeleineDialogue.text = "Madeleine? MADELEINE?! MADELEINE...!!";
            }
            else if (timer >= 120 && timer < 320)
            {
                MadeleineDialogue.text = "...Meu Deus, o que aconteceu com a minha esposa?!  Preciso encontrá-la...!";
            }
            else if (timer >= 320 && timer < 600)
            {
                MadeleineDialogue.text = "ANDAR: WASD - INTERAGIR: Clique Esquerdo do Mouse";
            }
            else if (timer >= 600 && timer < 1200)
            {
                MadeleineDialogue.text = "ACESSAR INVENTÁRIO: TAB - MENU PRINCIPAL: Esc";
            }
            else
            {
                MadeleineDialogue.text = "";
                countDialogue = false;
            }
        }

            


        if (!GameObject.Find("Main Menu Camera"))
        {
            if (!ActionContext.conActive && !EscapeMenu.isOpened)
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    if (isOpened)
                    {
                        isOpened = false;
                    }
                    else
                    {
                        isOpened = true;
                    }
                }

                //Desligar todo o movimento e ligar o mouse
                CharacterMotor.canControl = !isOpened;
                MouseLook.canUpdate = !isOpened;
                Screen.showCursor = isOpened;
            }
        }
    }



    //Inventário
    public void OnGUI()
    {
        GUI.skin = titanicSkin;

        if (isOpened)
        {
            //Fundinho Bonitin
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");

            //Botões
            for (int i = 0; i < aItens.Count; i++)
            {
                if (aItens[i].name == "Livro 1") tooltip = Livro1Tip;
                if (aItens[i].name == "Livro 2") tooltip = Livro2Tip;
                if (aItens[i].name == "Livro 3") tooltip = Livro3Tip;
                if (aItens[i].name == "Pedaço de Papel 1") tooltip = Papel1Tip;
                if (aItens[i].name == "Pedaço de Papel 2") tooltip = Papel2Tip;
                if (aItens[i].name == "Pedaço de Papel 3") tooltip = Papel3Tip;
                if (aItens[i].name == "Chave do Livro 2") tooltip = ChaveLivro2Tip;
                if (aItens[i].name == "Chave Mestra") tooltip = MasterChaveTip;

                if (i <= 2)
                {
                    if (GUI.Button(new Rect(Screen.width * 0.58f + i * (95 + 10), 100, 95, 76), new GUIContent(aItens[i].name, tooltip)))
                    {
                        //***COISAS ANTIGAS QUANDO ITENS AINDA SPAWNAVAM NA MÃO DO JOGADOR***
                        //isOpened = false;

                        //if (aItens[i].name != "Cruiser Map")
                        //{
                        //    handObj = Instantiate(aItens[i], handTarget.position, handTarget.rotation) as GameObject;
                        //    handObj.GetComponent<Rigidbody>().detectCollisions = false;
                        //    aItens.RemoveAt(i);
                        //}
                    }
                }
                else if (i >= 3 && i <= 5)
                {
                    if (GUI.Button(new Rect(Screen.width * 0.58f + (i - 3) * (195 + 10), 230, 95, 76), new GUIContent(aItens[i].name, tooltip)))
                    {
                    }
                }
                else if (i >= 6 && i <= 9)
                {
                    if (GUI.Button(new Rect(Screen.width * 0.58f + (i - 6) * (195 + 10), 360, 95, 76), new GUIContent(aItens[i].name, tooltip)))
                    {
                    }
                }
                else if (i >= 10 && i <= 12)
                {
                    if (GUI.Button(new Rect(Screen.width * 0.58f + (i - 9) * (195 + 10), 540, 95, 76), new GUIContent(aItens[i].name, tooltip)))
                    {
                    }
                }
            }


            //Mostrar Tooltip
            GUI.Label(new Rect(50, Screen.height * 0.73f, Screen.width - 100, 300), GUI.tooltip);

            //Ícones aparecem ao Mouseover
            if (GUI.tooltip == Papel1Tip)
            {
                GameObject.Find("Mapa 1 Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Mapa 1 Texture").GetComponent<GUITexture>().enabled = false;
            }

            if (GUI.tooltip == Papel2Tip)
            {
                GameObject.Find("Mapa 2 Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Mapa 2 Texture").GetComponent<GUITexture>().enabled = false;
            }

            if (GUI.tooltip == Papel3Tip)
            {
                GameObject.Find("Mapa 3 Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Mapa 3 Texture").GetComponent<GUITexture>().enabled = false;
            }

            if (GUI.tooltip == Livro1Tip)
            {
                GameObject.Find("Livro 1 Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Livro 1 Texture").GetComponent<GUITexture>().enabled = false;
            }

            if (GUI.tooltip == Livro2Tip)
            {
                GameObject.Find("Livro 2 Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Livro 2 Texture").GetComponent<GUITexture>().enabled = false;
            }

            if (GUI.tooltip == Livro3Tip)
            {
                GameObject.Find("Livro 3 Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Livro 3 Texture").GetComponent<GUITexture>().enabled = false;
            }

            if (GUI.tooltip == ChaveLivro2Tip)
            {
                GameObject.Find("Chave Pq Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Chave Pq Texture").GetComponent<GUITexture>().enabled = false;
            }

            if (GUI.tooltip == MasterChaveTip)
            {
                GameObject.Find("Master Chave Texture").GetComponent<GUITexture>().enabled = true;
            }
            else
            {
                GameObject.Find("Master Chave Texture").GetComponent<GUITexture>().enabled = false;
            }

        }
    }
}

