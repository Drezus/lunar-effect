﻿using UnityEngine;

class OpenDoor: MonoBehaviour
{
    public bool isOpened = false;
    public float closingDist = 5;
    public Transform playerPos;
    public GameObject doorTriggered;

    public AudioClip Abrir;
    public AudioClip Fechar;

    public void Update()
    {

        //Novo vetor de posição relativa para verificar a distância da porta aberta (gameObject) com o jogador:
        Vector3 dist = playerPos.position - gameObject.transform.position;

        if (!isOpened)
        {
            if (ActionContext.conActive && DistanceTrigger.triggeredObj.name == "Door")
            {
                doorTriggered = DistanceTrigger.triggeredObj;
                doorTriggered.animation.Play("door_open");
                doorTriggered.GetComponent<AudioSource>().PlayOneShot(Abrir);
                ActionContext.conActive = false;
                doorTriggered.GetComponent<OpenDoor>().isOpened = true;
            }
        }

        if (isOpened)
        {
            if (dist.magnitude > closingDist)
            {
                gameObject.animation.Play("door_close");
                gameObject.GetComponent<AudioSource>().PlayOneShot(Fechar);
                gameObject.GetComponent<OpenDoor>().isOpened = false;
                doorTriggered = null;
            }
        }

    }
}