﻿using UnityEngine;

class Switch2 : MonoBehaviour
{
    public bool isOn = false;
    public AudioClip Som;

    public void Update()
    {
        if (SlotLivro2.isFitted && !LightSolution.isCompleted)
        {

            //Ligar-desligar luzes
            if (ActionContext.conActive && DistanceTrigger.triggeredObj.name == "Switch 2")
            {
                ActionContext.conActive = false;
                DistanceTrigger.triggeredObj.GetComponent<AudioSource>().PlayOneShot(Som);

                if (isOn)
                {
                    isOn = false;
                }
                else
                {
                    isOn = true;
                }

                //Ligar/desligar as luzes especificamente afetadas por Switch 2
                GameObject.Find("Luz 2").GetComponent<Light>().enabled = !GameObject.Find("Luz 2").GetComponent<Light>().enabled;
                GameObject.Find("Luz 3").GetComponent<Light>().enabled = !GameObject.Find("Luz 3").GetComponent<Light>().enabled;
                GameObject.Find("Luz 6").GetComponent<Light>().enabled = !GameObject.Find("Luz 6").GetComponent<Light>().enabled;
                GameObject.Find("Luz 7").GetComponent<Light>().enabled = !GameObject.Find("Luz 7").GetComponent<Light>().enabled;
                GameObject.Find("Luz 8").GetComponent<Light>().enabled = !GameObject.Find("Luz 8").GetComponent<Light>().enabled;
                GameObject.Find("Luz 9").GetComponent<Light>().enabled = !GameObject.Find("Luz 9").GetComponent<Light>().enabled;
                GameObject.Find("Luz 11").GetComponent<Light>().enabled = !GameObject.Find("Luz 11").GetComponent<Light>().enabled;
                GameObject.Find("Luz 12").GetComponent<Light>().enabled = !GameObject.Find("Luz 12").GetComponent<Light>().enabled;
                GameObject.Find("Luz 13").GetComponent<Light>().enabled = !GameObject.Find("Luz 13").GetComponent<Light>().enabled;
                GameObject.Find("Luz 14").GetComponent<Light>().enabled = !GameObject.Find("Luz 14").GetComponent<Light>().enabled;
                GameObject.Find("Luz 16").GetComponent<Light>().enabled = !GameObject.Find("Luz 16").GetComponent<Light>().enabled;

            }
        }

    }
}