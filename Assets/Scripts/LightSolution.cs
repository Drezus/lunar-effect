﻿using UnityEngine;

class LightSolution : MonoBehaviour
{
    public static bool isCompleted = false;
    public AudioClip Barulhinho;

    public GameObject ChaveMestra;
    public Transform ChaveMestraPos;

    public void Update()
    {
        if (GameObject.Find("Luz 1").GetComponent<Light>().enabled == true && GameObject.Find("Luz 2").GetComponent<Light>().enabled == true && GameObject.Find("Luz 3").GetComponent<Light>().enabled == true && GameObject.Find("Luz 4").GetComponent<Light>().enabled == true && GameObject.Find("Luz 5").GetComponent<Light>().enabled == true && GameObject.Find("Luz 6").GetComponent<Light>().enabled == true && GameObject.Find("Luz 7").GetComponent<Light>().enabled == true && GameObject.Find("Luz 8").GetComponent<Light>().enabled == true && GameObject.Find("Luz 9").GetComponent<Light>().enabled == true && GameObject.Find("Luz 10").GetComponent<Light>().enabled == true && GameObject.Find("Luz 11").GetComponent<Light>().enabled == true && GameObject.Find("Luz 12").GetComponent<Light>().enabled == true && GameObject.Find("Luz 13").GetComponent<Light>().enabled == true && GameObject.Find("Luz 14").GetComponent<Light>().enabled == true && GameObject.Find("Luz 15").GetComponent<Light>().enabled == true && GameObject.Find("Luz 16").GetComponent<Light>().enabled == true && !isCompleted)
        {
            isCompleted = true;
            GameObject cm = GameObject.Instantiate(ChaveMestra, ChaveMestraPos.position, ChaveMestraPos.rotation) as GameObject;
            cm.GetComponent<AudioSource>().PlayOneShot(Barulhinho);
            cm.name = "Chave Mestra";
        }

    }
}