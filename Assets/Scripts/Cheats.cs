﻿using UnityEngine;

class Cheats : MonoBehaviour
{
    public GameObject ChaveMestra;
    public Transform ChaveMestraPos;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            GameObject.Find("Slot Livro 1").GetComponent<SlotLivro1>().hasBook = true;
            GameObject.Find("Slot Livro 2").GetComponent<SlotLivro2>().hasBook = true;
            GameObject.Find("Slot Livro 3").GetComponent<SlotLivro3>().hasBook = true;
        }

        if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            LightSolution.isCompleted = true;
            GameObject cm = GameObject.Instantiate(ChaveMestra, ChaveMestraPos.position, ChaveMestraPos.rotation) as GameObject;
            cm.name = "Chave Mestra";

            GameObject.Find("Luz 1").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 2").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 3").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 4").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 5").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 6").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 7").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 8").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 9").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 10").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 11").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 12").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 13").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 14").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 15").GetComponent<Light>().enabled = true;
            GameObject.Find("Luz 16").GetComponent<Light>().enabled = true;
        }
    }
}