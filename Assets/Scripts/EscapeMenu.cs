﻿using UnityEngine;

class EscapeMenu: MonoBehaviour
{
    public static bool isOpened = false;

    public void Start()
    {
        isOpened = false;
    }

    public void Update()
    {
        if (!GameObject.Find("Main Menu Camera"))
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !Inventory.isOpened && !ActionContext.conActive)
            {
                if (isOpened)
                {
                    isOpened = false;
                }
                else
                {
                    isOpened = true;
                }

                //Desligar todo o movimento e ligar o mouse
                CharacterMotor.canControl = !isOpened;
                MouseLook.canUpdate = !isOpened;
                Screen.showCursor = isOpened;
            }
        }
    }



    //Inventário
    public void OnGUI()
    {
        if (isOpened)
        {
            //Fundinho Bonitin
            GUI.Box(new Rect(Screen.width/2 - 100, Screen.height/2 - 200, 200, 240), "Menu Principal");

            //Botões
            if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height / 2 - 200 + 30, 120, 50), "Menu Principal"))
            {
                Application.LoadLevel(0);
            }

            //Botões
            if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height / 2 - 110 + 30, 120, 50), "Sair do Jogo"))
            {
                Application.Quit();
            }

        }
    }
}