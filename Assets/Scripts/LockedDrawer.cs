﻿using UnityEngine;

class LockedDrawer : MonoBehaviour
{
    public bool isUnlocked = false;
    public bool hasKey = false;
    public GameObject keyObj;
    public bool isOpened = false;
    public float closingDist = 5;
    public Transform playerPos;
    public GameObject drawerTriggered;

    public AudioClip Abrir;
    public AudioClip Destrancar;

    public void Start()
    {
        GameObject.Find("Livro 3").GetComponent<BoxCollider>().enabled = false;
    }

    public void Update()
    {
        //Novo vetor de posição relativa para verificar a distância da porta aberta (gameObject) com o jogador:
        Vector3 dist = playerPos.position - gameObject.transform.position;

        //Verificação constante pela chave
        for (int i = 0; i < Inventory.aItens.Count; i++)
        {
            if (Inventory.aItens[i].name == keyObj.name){hasKey = true;}
        }

        if (hasKey)
        {

            if (!isOpened)
            {
                if (ActionContext.conActive && DistanceTrigger.triggeredObj.name == "Locked Drawer")
                {
                    drawerTriggered = DistanceTrigger.triggeredObj;
                    drawerTriggered.animation.Play("drawer_open");
                    drawerTriggered.GetComponent<AudioSource>().PlayOneShot(Destrancar);
                    drawerTriggered.GetComponent<AudioSource>().PlayOneShot(Abrir);
                    ActionContext.conActive = false;
                    drawerTriggered.GetComponent<LockedDrawer>().isOpened = true;

                    GameObject.Find("Livro 3").GetComponent<BoxCollider>().enabled = true;

                    if (!isUnlocked)
                    { 
                        isUnlocked = true;

                        //Tirar a chave da Array do Inventário
                        for (int i = 0; i < Inventory.aItens.Count; i++)
                        {
                            if (Inventory.aItens[i].name == "Chave do Livro 2") { Inventory.aItens.RemoveAt(i); Inventory.aItens.TrimExcess(); }
                        }
                    }
                    
                }
            }

            if (isOpened)
            {
                if (dist.magnitude > closingDist)
                {
                    gameObject.animation.Play("drawer_close");
                    gameObject.GetComponent<AudioSource>().PlayOneShot(Abrir);
                    gameObject.GetComponent<LockedDrawer>().isOpened = false;
                    drawerTriggered = null;
                }
            }
        }
    }
}